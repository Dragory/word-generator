<!doctype html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Word Relations and Generator</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div id="menu">
        <ul class="menu-list">
<?php
    foreach ($sourceFiles as $key => $file)
    {
        echo '<li class="menu-item">'.
                '<a href="?source='.$key.'">'.$file[1].'</a>'.
                ' <a href="'.$file[0].'"><small>(orig)</small></a>'.
             '</li>';
    }
?>
        </ul>
    </div>
    <div id="wrap">
        <h1>Generated words</h1>
        <p>
            <strong>Source:</strong> <?= $sourceFile[1] ?>
        </p>
        <p>
<?php
    $output = '';

    for ($i = 0; $i < 10; $i++)
    {
        $sentence = $sentences[array_rand($sentences)];
        $words = [];

        for ($a = 0; $a < $sentence[1]; $a++)
        {
            $words[] = $generator->generateWord();
            $profiler->addStep('$generator->generateWord()');
        }

        $sentenceString = ucfirst(vsprintf($sentence[0], $words));
        $sentenceString = preg_replace_callback('/[.!?]\s*?([^\s]{1})/isu', function($matches) {
            return mb_strtoupper($matches[0]);
        }, $sentenceString);

        $output .= $sentenceString.'<br>';
    }

    echo $output;
?>
        </p>

        <h1>Execution times</h1>
        <table>
            <thead>
                <tr>
                    <th>Time (ms)</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
        <?php
            foreach ($profiler->steps as $step)
            {
                echo '<tr>'.
                        '<td>'.number_format($step[2]*1000, 4).'</td>'.
                        '<td>'.$step[0].'</td>'.
                     '</tr>';
            }
        ?>
            </tbody>
        </table>

        <h1>Debug information</h1>
        <pre>
<?php
    echo $generator->getWordShapeCount().'<br><br>';

    $shapes = $generator->getWordShapes();
    arsort($shapes);
    print_r($shapes);
?>
        </pre>
    </div>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</body>
</html>