<?php

/**
 * HEADERS AND PHP SETTINGS
 */

header('Content-Type: text/html; charset=UTF-8');
mb_internal_encoding('UTF-8');
ini_set('display_errors', 'On');
error_reporting(E_ALL);

/**
 * HELPERS
 */

// Thanks to http://stackoverflow.com/a/2518021/316944
function mb_ucfirst($string, $encoding = null)
{
    if (!$encoding) $encoding = mb_internal_encoding();

    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

// Loads a template file and passes it data
function loadTemplate($template, $data = [])
{
    $__data = $data;
    extract($__data);
    include($template);
}

/**
 * REQUIRED FILES
 */

require_once('WordGenerator.class.php');
require_once('Profiler.class.php');

/**
 * PROCESSING
 */

// Start the profiler
$profiler = new Profiler;

// Our source text files
$sourceFiles = [
    ['sources/source.txt', 'Seitsemän veljestä (short)'],
    ['sources/source2.txt', 'Reddit comments'],
    ['sources/source3.txt', 'Wikipedia (EN) on the English language'],
    ['sources/source4.txt', 'Wikipedia (FI) on Suomi'],
    ['sources/source5.txt', 'Dragory/Soyuz'],
    ['sources/source6.txt', 'Game names'],
    ['sources/veljekset.txt', 'Seitsemän veljestä (long)'],
    ['sources/source9.txt', 'Nicknames'],
    ['sources/kunnat.txt', 'Finnish municipalities']
];

$source = (isset($_GET['source']) ? intval($_GET['source']) : 0);

if (isset($sourceFiles[$source])) $sourceFile = &$sourceFiles[$source];
else $sourceFile = &$sourceFiles[0];

// Get the source text and parse the text
$sourceText = file_get_contents($sourceFile[0]);
$profiler->addStep('file_get_contents');

$generator = new WordGenerator;
$profiler->addStep('$generator = new WordGenerator;');

$generator->parseText($sourceText);
$profiler->addStep('$generator->parseText($sourceText);');

// Possible sentences
$sentences = [
    ['%s %s %s, %s %s.', 5],
    ['%s %s! %s... %s %s.', 5],
    ['%s %s %s? %s!', 4]
];

// Load the template
loadTemplate('template.php', [
    'generator' => $generator,
    'sourceFiles' => $sourceFiles,
    'sourceFile' => $sourceFile,
    'profiler' => $profiler,
    'sentences' => $sentences
]);