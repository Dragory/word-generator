<?php

class WordGenerator
{
    protected $wordShapes            = [],
              $wordShapeTotal        = 0,
              $letterRelations       = [],
              $letterRelationsTotals = [],

              $vowels       = 'aeiouyäö',
              $truncatedLetters = '-–\'', // Letters that will be removed instead of converted to whitespace
              $validLetters = 'abcdefghijklmnopqrstuvwxyzåäöÅÄÖ ';

    public $stats = [];

    public function __construct() {}

    /**
     * Set which letters are vowels.
     * @param string $vowels The vowels
     */
    public function setVowels($vowels)
    {
        $this->vowels = $vowels;
    }

    public function parseText($text)
    {
        // Change the vowels and valid letters into arrays
        $this->vowels           = preg_split('//u', $this->vowels, -1, PREG_SPLIT_NO_EMPTY);
        $this->validLetters     = preg_split('//u', $this->validLetters, -1, PREG_SPLIT_NO_EMPTY);
        $this->truncatedLetters = preg_split('//u', $this->truncatedLetters, -1, PREG_SPLIT_NO_EMPTY);

        // Do we have this cached? If so, get the cached data.
        $md5       = md5($text);
        $cacheFile = 'cache/cached_'.$md5;

        $letterRelations = [];
        $wordShapes      = [];

        // If we have this text's parsing already cached, use the cached data
        if (file_exists($cacheFile))
        {
            $cacheData = unserialize(file_get_contents($cacheFile));

            // Use the data from the cache
            $letterRelations = $cacheData['relatedLetters'];
            $wordShapes = $cacheData['wordShapes'];
        }
        // Otherwise calculate the data
        else
        {
            // Replace non-letters
            $letters = preg_split('//u', $text, -1, PREG_SPLIT_NO_EMPTY);
            $tempText = '';

            foreach ($letters as $letter)
            {
                if (in_array($letter, $this->truncatedLetters)) $tempText .= '';
                elseif (in_array($letter, $this->validLetters)) $tempText .= $letter;
                else $tempText .= ' ';
            }

            $text = $tempText;

            // Dash is separately as it can be used IN a word
            $text = str_replace(['-', '–'], '', $text);

            // Remove excessive whitespace
            $text = preg_replace('/[\s+]/is', ' ', $text);

            // Split the text into words
            $words = explode(' ', $text);

            // Parse the words
            foreach ($words as $word)
            {
                // Make sure the word isn't empty
                $word = trim($word);
                if (empty($word)) continue;

                // Get the relations and shape of the word
                list($thisLetterRelations, $thisWordShape) = $this->parseWord($word);

                // Merge the shape with out current data
                $wordShapeArray = preg_split('//u', $thisWordShape, -1, PREG_SPLIT_NO_EMPTY);

                if (count($wordShapeArray) > 3)
                {
                    if (!isset($wordShapes[$thisWordShape])) $wordShapes[$thisWordShape] = 0;
                    $wordShapes[$thisWordShape]++;
                }

                // Merge the relations with out current data
                foreach ($thisLetterRelations as $letter => $relations)
                {
                    // Make sure we have this letter in the relations array
                    if (!isset($letterRelations[$letter])) $letterRelations[$letter] = [];

                    foreach ($relations as $relatedLetter => $count)
                    {
                        // Make sure we have a place for the related letter
                        if (!isset($letterRelations[$letter][$relatedLetter])) $letterRelations[$letter][$relatedLetter] = 0;
                        $letterRelations[$letter][$relatedLetter] += $count;
                    }
                }
            }

            // Save the data to the cache
            $cacheData = [
                'relatedLetters' => &$letterRelations,
                'wordShapes' => &$wordShapes
            ];

            file_put_contents($cacheFile, serialize($cacheData));
        }

        // Sort the letter relations by the most common letters
        foreach ($letterRelations as $letter => &$relations)
        {
            arsort($relations);

            // And leave the top 50% of the relations
            $top = ceil(count($relations) * 0.5);
            $relations = array_slice($relations, 0, $top);
        }

        // Get the count of of the relations and shapes
        $this->wordShapes     = $wordShapes;
        $this->wordShapeTotal = array_sum($wordShapes);

        $totalTotalLetterRelations = 0; // <- This is for stats

        $this->letterRelations = $letterRelations;
        foreach ($letterRelations as $letter => $relations)
        {
            $count = 0;

            foreach ($relations as $relatedLetter => $relatedLetterCount)
            {
                $count += $relatedLetterCount;
            }

            $this->letterRelationsTotals[$letter] = $count;
            $totalTotalLetterRelations += $count;
        }

        // Save some stats
        $this->stats['wordShapes'] = $this->wordShapeTotal;
        $this->stats['wordShapesUnique'] = count($wordShapes);
        // $this->stats['wordShapesSum'] = $this->wordShapeTotal;
        $this->stats['letterRelations'] = $totalTotalLetterRelations;

        return true;
    }

    /**
     * Parses a given word and returns its letterRelations
     * and wordShape.
     * @param  string $word  The given word
     * @return array         An array containing the letter relations at 0 and word shape at 1.
     */
    protected function parseWord($word)
    {
        // Get the word shape and save it if it's not already saved
        $wordShape = $this->getWordShape($word);
        $wordShapeArray = preg_split('//u', $wordShape, -1, PREG_SPLIT_NO_EMPTY);

        if (count($wordShapeArray) > 3 || true)
        {
            if (!isset($this->wordShapes[$wordShape])) $this->wordShapes[$wordShape] = 0;
            $this->wordShapes[$wordShape]++;
        }

        // Get the letter relations and merge them with out current relations
        $letterRelations = $this->getLetterRelations($word);

        foreach ($letterRelations as $letter => $relations)
        {
            // Make sure we have this letter in the relations array
            if (!isset($this->letterRelations[$letter])) $this->letterRelations[$letter] = [];

            foreach ($relations as $relatedLetter => $count)
            {
                // Make sure we have a place for the related letter
                if (!isset($this->letterRelations[$letter][$relatedLetter])) $this->letterRelations[$letter][$relatedLetter] = 0;
                $this->letterRelations[$letter][$relatedLetter] += $count;
            }
            // $this->letterRelationsRaw[$letter] = array_merge($this->letterRelationsRaw[$letter], $relations);
        }

        // Return the results
        return [$letterRelations, $wordShape];
    }

    /**
     * Gets the given word's "shape".
     * @param  string $word The given word
     * @return string       The given word's shape
     */
    protected function getWordShape($word)
    {
        $wordShape = '';
        $wordArray = preg_split('//u', $word, -1, PREG_SPLIT_NO_EMPTY);

        foreach ($wordArray as $letter)
        {
            if ($this->isVowel($letter))
                $wordShape .= 'a';
            else
                $wordShape .= 'b';
        }

        return $wordShape;
    }

    /**
     * Turns a letterRelations array into a "raw" letterRelations
     * array, i.e. every word shape is repeated for the amount
     * its count was in the given letterRelations array.
     * @return array  The raw letterRelations array
     */
    protected function letterRelationsToRaw($data)
    {
        $raw = [];

        foreach ($data as $letter => $relations)
        {
            foreach ($relations as $relatedLetter => $count)
            {
                for ($i = 0; $i < $count; $i++)
                {
                    $raw[$letter][] = $relatedLetter;
                }
            }
        }

        return $raw;
    }

    /**
     * Turns a wordShapes array into a "raw" wordShapes
     * array, i.e. every word shape is repeated for the amount
     * its count was in the given wordShapes array.
     * @return array  The raw wordShapes array
     */
    protected function wordShapesToRaw($data)
    {
        $raw = [];

        foreach ($data as $shape => $count)
        {
            for ($i = 0; $i < $count; $i++)
            {
                $raw[] = $shape;
            }
        }

        return $raw;
    }

    /**
     * Gets the letter relations for the given word.
     * @param  string $word The given word
     * @return array        The letter relations
     */
    protected function getLetterRelations($word)
    {
        $relations = [];

        $letters = preg_split('//u', $word, -1, PREG_SPLIT_NO_EMPTY);
        $letterCount = count($letters);

        foreach ($letters as $key => $letter)
        {
            $letter = mb_strtolower($letter);
            if (!isset($relations[$letter])) $relations[$letter] = [];

            // If we have a following letter
            if ($key + 1 < $letterCount)
            {
                $relatedLetter = mb_strtolower($letters[$key + 1]);

                if (!isset($relations[$letter][$relatedLetter])) $relations[$letter][$relatedLetter] = 0;
                $relations[$letter][$relatedLetter]++;
            }
        }

        return $relations;
    }

    /**
     * Returns whether the given letter is a vowel.
     * @param  string  $letter The letter
     * @return boolean         Whether the letter's a vowel
     */
    protected function isVowel($letter)
    {
        return in_array(mb_strtolower($letter), $this->vowels);
    }

    protected function randWeighted(&$vals, &$count)
    {
        $num = mt_rand(0, $count);
        foreach ($vals as $key => $val)
        {
            $num -= $val;
            if ($num <= 0) return $key;
        }
    }

    /**
     * Generates a word based on the analyzed text(s).
     * @return string The generated word
     */
    public function generateWord()
    {
        $shape = $this->randWeighted($this->wordShapes, $this->wordShapeTotal);
        // $shape = $this->wordShapes[array_rand($this->wordShapesRaw)];
        $shapeLetters = str_split($shape);
        
        // var_dump($shape);

        $word = '';
        $previousLetter = '';
        foreach ($shapeLetters as $letter)
        {
            // Are we looking for a vowel?
            $isVowel = ($letter == 'a');

            // Make sure there aren't any leftovers from the previous loop
            $letters = [];
            $letterCount = 0;

            // If we have a previous letter, get its related letters
            if ($previousLetter)
            {
                // If we have relations for this letter, use those
                if (isset($this->letterRelations[$previousLetter]))
                {
                    $letters = $this->letterRelations[$previousLetter];
                }
            }

            // If no letter was found, construct a new array
            // that contains an equal chance for every letter.
            if (!$letters)
            {
                $temp = array_keys($this->letterRelations);
                // $letterCount = count($temp);

                foreach ($temp as $tempLetter)
                {
                    $letters[$tempLetter] = 1;
                }
            }

            // Are we looking for a vowel or not?
            $validLetters = [];
            foreach ($letters as $testLetter => $num)
            {
                // If we're looking for a vowel and this letter is a vowel
                // Or if we're looking for a consonant and this letter is a consonant
                if ($isVowel == $this->isVowel($testLetter))
                {
                    $validLetters[$testLetter] = $num;
                }
            }

            // If no letters were found (ehm), use an "a"
            if (empty($validLetters))
            {
                $validLetters['a'] = 1;
            }

            $letterCount = array_sum($validLetters);

            // Get the random letter
            $randomLetter = $this->randWeighted(
                $validLetters,
                $letterCount
            );

            // $addedLetter = $validLetters[$randomLetterKey];

            // Add a random letter from the valid letters to the word
            // $addedLetter = $validLetters[array_rand($validLetters)];
            // echo 'ADDING LETTER: '.$addedLetter."\n";
            $word .= $randomLetter;
            $previousLetter = $randomLetter;
        }

        return mb_strtolower($word);
    }

    public function getWordShapes()
    {
        return $this->wordShapes;
    }

    public function getWordShapeCount()
    {
        return $this->wordShapeTotal;
    }
}