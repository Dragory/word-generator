<?php

class Profiler
{
    private $start;
    public $steps = [];

    public function __construct()
    {
        $this->start = microtime(true);
    }

    public function addStep($text)
    {
        $time = microtime(true);
        $this->steps[] = [$text, $time, $time - $this->start];
    }
}